##################################################
# created by: Andrea Casati

##################################################

# get current user
export CURRENT_USER=`logname`

# user folder path
export USER_FOLDER_PATH=/mnt/DATA/${CURRENT_USER^}

# set root path
export PIPE_ROOT_PATH=$USER_FOLDER_PATH/Corsi/ar_PythonAdvanced

# add external paths to pythonpath
export PYTHONPATH=$PYTHONPATH:$USER_FOLDER_PATH/Progetti/Maya-Script:$USER_FOLDER_PATH/Progetti/Maya-Plugin

# add customs MAYA_SCRIPT_PATH
export MAYA_SCRIPT_PATH=$USER_FOLDER_PATH/Progetti/Maya-Script

# add customs MAYA_PLUG_IN_PATH
export MAYA_PLUG_IN_PATH=$USER_FOLDER_PATH/Progetti/Maya-Plugin

# define maya version
export MAYA_VERSION=2022

#disable autodesk CIP
export MAYA_DISABLE_CIP=1

# run maya
/usr/autodesk/maya2022/bin/maya &