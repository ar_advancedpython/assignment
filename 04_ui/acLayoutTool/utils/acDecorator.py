import time

def chronometer(func):

    def wrapper(*args, **kwargs):
        start = time.time()
        if args:
            func(args)
        else:
            func()
        end = time.time()
        message = f'{func.__name__} - Exec in : {(end - start).__round__(2)} seconds'
        print(message)


