import os
import re
import sys

from maya import mel, cmds
from maya import OpenMaya as om, OpenMayaUI as omui
from maya.api import OpenMaya as om2

def get_file_info():
    """
    Return generic informations about the file
    Returns:
        str: file_path
        str: file_name
        str: extension
        str: sequence_folder_path
        str: episode_folder_path

    """
    file_path = om.MFileIO_currentFile()
    file_name = os.path.basename(file_path)
    extension = os.path.splitext(file_path)[-1]
    sequence_folder_path = os.path.dirname(file_path)
    episode_folder_path = os.path.dirname(sequence_folder_path)

    return file_path, file_name, extension, sequence_folder_path, episode_folder_path

def get_top_reference_grp():
    """
    Get all the top group reference in the scene as a generator object
    Returns:
        generator: top_reference_grp
    """
    iterDag = om2.MItDag()
    for dagObj in iterDag.iter():
        mobj = dagObj.currentItem()
        dependNodeFn = om2.MFnDependencyNode()
        dependNodeFn.setObject(mobj)
        if dependNodeFn.isFromReferencedFile and mobj.apiTypeStr == "kTransform":
            dagFn = om2.MFnDagNode()
            dagFn.setObject(mobj)
            top_reference_grp = "|".join(dagObj.fullPathName().split("|")[:3])
            yield top_reference_grp

def get_scene_top_grp():
    """
    Get all the outliner top group and return them as a generator objects
    Returns:
        generator: top_scene_object
    """
    top_scene_object = (
        grp
        for grp in cmds.ls(assemblies=True)
        if grp.endswith("_grp") and
           cmds.objectType(grp, isType="transform")
    )
    return top_scene_object
