#********************************************************************
# content = Manage a layout sequence scene in maya
#
# version = 0.0.1
# date = 05/12/2022
#
# how to =
# dependencies = maya, PySide2
# todos =
#
# license = MIT
# author = Andrea Casati
#********************************************************************

import os
import re
import sys
import importlib
from pprint import pprint

from PySide2 import QtWidgets, QtCore, QtGui
from shiboken2 import wrapInstance

from maya import cmds, mel
from maya import OpenMaya as om
from maya import OpenMayaUI

sys.path.append(os.path.dirname(__file__))

from utils.Qt import QtWidgets, QtCore, QtGui, QtCompat

from utils import acDecorator, acUtil
importlib.reload(acDecorator)
importlib.reload(acUtil)
import modules.yaml as yaml

WINDOW_TITLE = "acLayoutTool"
WORKSPACE_NAME = "acLayoutTool_ws"

def define_workspace():
    """
    Create a maya workspace window and convert it to QWidget
    Returns:
       QWidget: QtWidgets.QWidget object
    """
    if cmds.workspaceControl(WORKSPACE_NAME, exists=True, query=True):
        cmds.deleteUI(WORKSPACE_NAME)
    window_control = cmds.workspaceControl(WORKSPACE_NAME, label=WINDOW_TITLE)
    qt_control = OpenMayaUI.MQtUtil.findControl(window_control)
    pointer = wrapInstance(int(qt_control), QtWidgets.QWidget)
    return pointer


class LayoutToolUI(QtWidgets.QWidget):
    """
    LayoutToolUI generate two different UIs,
    1 - to init the scene based on a tbl file
    2 - to manage the sequence scene and export it subdivided in shots
    """

    def __init__(self):
        parent = define_workspace()
        super(LayoutToolUI, self).__init__(parent=parent)

        self.configuration_path = os.path.join(os.path.dirname(__file__), "config/config.yml")

        ##################################################
        # context
        self.file_path, self.file_name, self.file_extension, self.sequence_folder_path, self.episode_folder_path = acUtil.get_file_info()

        # TODO: set "already_initialized" to false before deploying
        self.already_initialized = True
        if ".sequence_initialized" in os.listdir(self.sequence_folder_path):
            self.already_initialized = True

        ##################################################
        # init the ui file
        tool_ui = os.path.join(os.path.dirname(__file__), "ui/layoutTool.ui")
        self.ui = QtCompat.loadUi(tool_ui)

        self.main_layout = QtWidgets.QVBoxLayout(self)
        self.main_layout.addWidget(self.ui)

        if not self.already_initialized:
            self.initSequenceWidget_setup(True)
            self.manageSequenceWidget_setup(False)
        else:
            self.initSequenceWidget_setup(False)
            self.manageSequenceWidget_setup(True)

        self._signal_setup()

        ##################################################
        # add current QWidget to the layout of the parent window
        self.parent().layout().addWidget(self)

    ##################################################
    # UI setup
    def initSequenceWidget_setup(self, visibility):
        """
        Setup the initSequenceWidget
        Args:
            visibility: bool

        Returns:
            bool
        """
        try:
            self.ui.initSequenceWidget.setVisible(visibility)
            if visibility==True:
                return True
        except:
            return False


    def manageSequenceWidget_setup(self, visibility):
        """
        Setup the manageSequenceWidget
        Args:
            visibility: bool

        Returns:
            bool
        """
        try:
            self.ui.manageSequenceWidget.setVisible(visibility)
            if visibility==True:
                qframes = [
                    self.ui.sequencePreviewFrame,
                    self.ui.sceneManagerWidget
                ]

                splitter = QtWidgets.QSplitter()
                splitter.setOrientation(QtCore.Qt.Vertical)
                for frame in qframes:
                    splitter.addWidget(frame)

                self.ui.manageSequenceLay.addWidget(splitter)

                self.populate_scene_object_lists()

                # TODO: create a sequence camera driven from all the shots cameras
                self._set_cameraView_panel('tst_000_001:cam')
                return True
        except:
            return False

    def populate_scene_object_lists(self):
        """
        Populate the cameraListView, characterListView, propListView, setListView
        Returns:
            Bool
        """
        try:
            config = self.outliner_hierarchy

            camera_model = QtGui.QStandardItemModel()
            character_model = QtGui.QStandardItemModel()
            prop_model = QtGui.QStandardItemModel()
            sets_model = QtGui.QStandardItemModel()

            for asset in set(acUtil.get_top_reference_grp()):
                item = QtGui.QStandardItem()
                asset_name = asset.split(":")[0].split("|")[-1]
                asset_dag = asset
                item.setText(asset_name)
                item.setData(asset_dag, 3)

                if config[self.ui.cameraListView.objectName()] == cmds.listRelatives(asset, parent=True)[0]:
                    camera_model.appendRow(item)
                if config[self.ui.characterListView.objectName()] == cmds.listRelatives(asset, parent=True)[0]:
                    character_model.appendRow(item)
                if config[self.ui.propListView.objectName()] == cmds.listRelatives(asset, parent=True)[0]:
                    prop_model.appendRow(item)
                if config[self.ui.setListView.objectName()] == cmds.listRelatives(asset, parent=True)[0]:
                    sets_model.appendRow(item)

            self.ui.cameraListView.setModel(camera_model)
            self.ui.characterListView.setModel(character_model)
            self.ui.propListView.setModel(prop_model)
            self.ui.setListView.setModel(sets_model)

            return True

        except:
            return False

    def _add_cameraView_panel(self, camera=None):
        """
        Get the maya camera panel c++ pointer and convert it to a QWidget
        Args:
            camera: camera name

        Returns:
            QWidget: pointer
            Maya UI Object: barLay
        """
        if not camera:
            om.MGlobal_displayError("No camera to preview")
            return None, None

        model = cmds.modelPanel(camera=camera) # eg. tst_000_001_0020:cam
        barLay = cmds.modelPanel(model, query=True, barLayout=True)
        qt_control = OpenMayaUI.MQtUtil.findControl(model)
        pointer = wrapInstance(int(qt_control), QtWidgets.QWidget)

        return pointer, barLay

    def _set_cameraView_panel(self, camera):
        """
        Assign the camera view widget to the LayoutTool sequenceView_lay
        Delete extra panel bars
        Args:
            camera: camera name

        Returns:
            Bool
        """
        pointer, bar = self._add_cameraView_panel(camera)
        if all([pointer==None, bar==None]):
            self.ui.sequenceView_lay.addWidget(QtWidgets.QWidget())
            return
        else:
            self.ui.sequenceView_lay.addWidget(pointer)
            cmds.deleteUI(bar.split("|")[-2])
            cmds.deleteUI(bar.split("|")[-1])
            return True
        
    ##################################################
    # UI Signal
    def _signal_setup(self):
        self.ui.autoFillBtn.clicked.connect(self.autoFillBtn_func)

    ##################################################
    # UI Slot
    def autoFillBtn_func(self):
        """
        Auto generate and fill the TBL file path
        Returns:
            str: tbl_file_path
        """
        try:
            TBL_folder = os.path.join(self.episode_folder_path, "TBL")
            self.TBL_path = sorted([tbl_file.path for tbl_file in os.scandir(TBL_folder) if tbl_file.is_file()])[-1]
            self.ui.tBLPathLineEdit.setText(self.TBL_path)
            return self.TBL_path
        except Exception as e:
            om.MGlobal_displayError(e)
            return False

    ##################################################
    # get configuration info
    def read_config(self):
        with open(self.configuration_path, "r") as conf:
            data = yaml.safe_load(conf)

        self.config_data = data
        return self.config_data

    @property
    def project(self):
        return self.read_config()["project"]["name"]

    @property
    def fps(self):
        return self.read_config()["project"]["fps"]

    @property
    def playblast_res(self):
        return self.read_config()["playblast_resolution"]

    @property
    def outliner_hierarchy(self):
        return self.read_config()["outliner_hierarchy"]




