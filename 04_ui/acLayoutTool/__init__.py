import os
import sys

module_folder = os.path.join(os.path.dirname(__file__), "modules")
sys.path.append(module_folder)
for module in os.listdir(module_folder):
    sys.path.append(os.path.join(module_folder, module))