# ADVANCED ***************************************************************************
# content = assignment
#
# date    = 2022-01-07
# email   = contact@alexanderrichtertd.com
#************************************************************************************

"""
CUBE CLASS

1. CREATE an abstract class "Cube" with the functions:
   translate(x, y, z), rotate(x, y, z), scale(x, y, z) and color(R, G, B)
   All functions store and print out the data in the cube (translate, rotate, scale and color).

2. ADD an __init__(name) and create 3 cube objects.

3. ADD the function print_status() which prints all the variables nicely formatted.

4. ADD the function update_transform(ttype, value).
   "ttype" can be "translate", "rotate" and "scale" while "value" is a list of 3 floats.
   This function should trigger either the translate, rotate or scale function.

   BONUS: Can you do it without using ifs?

5. CREATE a parent class "Object" which has a name, translate, rotate and scale.
   Use Object as the parent for your Cube class.
   Update the Cube class to not repeat the content of Object.

"""

class Cube:
    cube_translate = [0, 0, 0]
    cube_rotate = [0, 0, 0]
    cube_scale = [1, 1, 1]
    cube_color = [90, 90, 90]

    def __init__(self, name):
        self.name = name
        self.print_status()

    def translate(self, args):
        if type(args)!=list:
            print("Error!")
            return False
        if not args:
            return self.cube_translate
        else:
            self.cube_translate = args
            print(f"New translation: {args}")
            return self.cube_translate

    def rotate(self, *args):
        if not args:
            return self.cube_rotate
        else:
            self.cube_rotate = args
            print("New")
            return self.cube_rotate

    def scale(self, *args):
        if not args:
            return self.cube_scale
        else:
            self.cube_scale = args
            print("New")
            return self.cube_scale

    def color(self, args=None):
        if not args:
            return self.cube_color
        else:
            self.cube_color = args
            print("New")
            return self.cube_color

    def update_transform(self, ttype, value):
        eval(f"self.{ttype}({value})")

    def print_status(self):
        info = [
            f"Name : {self.name}",
            f"Translate : {self.cube_translate}",
            f"Rotate : {self.cube_rotate}",
            f"Scale : {self.cube_scale}",
            f"Color : {self.cube_color}"
        ]

        print("\n".join(info))

        return True

c = Cube("Cubo")
c.translate([12, 0.4, 1])
c.print_status()
c.update_transform("translate", [1, 2, 3])