# STYLE ***************************************************************************
# content = assignment (Python Advanced)
#
# date    = 2022-30-10
# email   = andrea.casati@live.it
#**********************************************************************************

import traceback

import maya.cmds as mc

def set_color(ctrlList=None, color=None):
    '''
    Change the color to the objects listed

    :param ctrlList: [List] objects to edit
    :param color: [int] color parameter

    :return: [bool]
    '''
    color_map = {
        "1": 4,
        "2": 13,
        "3": 25,
        "4": 17,
        "5": 17,
        "6": 15,
        "7": 6,
        "8": 16
    }

    for ctrlName in ctrlList:
        try:
            mc.setAttr(ctrlName + 'Shape.overrideEnabled', 1)
        except:
            error = traceback.format_exc()
            mc.error(error)

        try:
            mc.setAttr(ctrlName + 'Shape.overrideColor', color_map[str(color)])
        except:
            error = traceback.format_exc()
            mc.error(error)

    return True

# EXAMPLE
# set_color(['circle','circle1'], 8)
