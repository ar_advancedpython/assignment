# STYLE ***************************************************************************
# content = assignment
#
# date    = 2022-30-10
# email   = andrea.casati@live.it
# **********************************************************************************

import os

def findCaller(self):
    """
    Find the stack frame of the caller so that we can note the source
    file name, line number and function name.
    """

    rv = "(unknown file)", 0, "(unknown function)"
    f = currentframe()  # None if IronPython isn't run with -X:Frames.

    if f == None:
        f = f.f_back

    while hasattr(f, "f_code"):
        co = f.f_code
        filename = os.path.normcase(co.co_filename)

        if filename == _srcfile:
            f = f.f_back
            continue

        rv = (co.co_filename, f.f_lineno, co.co_name)

        break

    return rv
