#********************************************************************
# content = Manage a layout sequence scene in maya
#
# version = 0.0.1
# date = 12/11/2022
#
# how to =
# dependencies = maya, PySide2
# todos =
#
# license = MIT
# author = Andrea Casati
#********************************************************************

import sys
import os
import re
from pprint import pprint

from PySide2 import QtWidgets, QtCore, QtGui
from shiboken2 import wrapInstance

from maya import cmds, mel
from maya import OpenMaya
from maya import OpenMayaUI

sys.path.append(os.path.dirname(__file__))

from utils.Qt import QtWidgets, QtCore, QtGui, QtCompat

from utils import acDecorator, acUtil
import modules.yaml as yaml

WINDOW_TITLE = "acLayoutTool"
WORKSPACE_NAME = "acLayoutTool_ws"

def define_workspace():
    """
    Create a maya workspace to integrate at the best the custom ui
    with the maya one
    Returns:
        c++ pointer
    """
    if cmds.workspaceControl(WORKSPACE_NAME, exists=True, query=True):
        cmds.deleteUI(WORKSPACE_NAME)
    window_control = cmds.workspaceControl(WORKSPACE_NAME, label=WINDOW_TITLE)
    qt_control = OpenMayaUI.MQtUtil.findControl(window_control)
    pointer = wrapInstance(int(qt_control), QtWidgets.QWidget)
    return pointer


class LayoutToolUI(QtWidgets.QWidget):
    """
    LayoutToolUI generate two different UIs,
    1 - to init the scene based on a tbl file
    2 - to manage the sequence scene and export it subdivided in shots
    """

    def __init__(self):
        parent = define_workspace()
        super(LayoutToolUI, self).__init__(parent=parent)

        # configuration path
        self.config_path = os.path.join(os.path.dirname(__file__), "config/config.yml")

        # ui paths
        tool_ui = os.path.join(os.path.dirname(__file__), "ui/layoutTool.ui")
        self.ui = QtCompat.loadUi(tool_ui)

        self.lay = QtWidgets.QVBoxLayout(self)
        self.lay.addWidget(self.ui)

        # self.set_cameraView_panel()


        print(self.project)
        print(self.fps)
        print(self.playblast_res)

        # add current QWidget to the layout of the parent window
        self.parent().layout().addWidget(self)

    def read_config(self):
        with open(self.config_path, "r") as conf:
            data = yaml.safe_load(conf)

        self.config_data = data
        return self.config_data

    @property
    def project(self):
        return self.read_config()["project"]["name"]

    @property
    def fps(self):
        return self.read_config()["project"]["fps"]

    @property
    def playblast_res(self):
        return self.read_config()["playblast_resolution"]

    def init_ui(self):
        pass

    def manage_ui(self):
        pass

    def add_cameraView_panel(self, camera=None):
        if not camera:
            return None, None

        model = cmds.modelPanel(camera='tst_000_001_0020:cam')
        barLay = cmds.modelPanel(model, query=True, barLayout=True)
        qt_control = OpenMayaUI.MQtUtil.findControl(model)
        pointer = wrapInstance(int(qt_control), QtWidgets.QWidget)

        return pointer, barLay

    def set_cameraView_panel(self):
        pointer, bar = self.add_cameraView_panel()
        if all([pointer==None, bar==None]):
            self.ui.verticalLayout_3.addWidget(QtWidgets.QWidget())
            return
        else:
            self.ui.verticalLayout_3.addWidget(pointer)
            cmds.deleteUI(bar.split("|")[-2])
            cmds.deleteUI(bar.split("|")[-1])
            return True




