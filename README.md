# acLayoutTool
## Author: Andrea Casati
## Status: WIP

### Description:
A maya tool to manage multiple shots in a single scene.\
It will be helpful in layout dpt. that work with a sequence workflow and shot workflow.\
### Main features:
- create and manage shots in timeline
- nice preview window to check continuty between each shots
- manage assets in scene: import/remove, define which assets are in which shots
- define shots timing: add/remove frames without break up animations
- camera bake&export
- shot exporter: export only one shot, or multiple once
